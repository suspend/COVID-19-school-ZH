# Step 0 - Setup and auxiliary functions ---------------------------------------
source("setup.R")
source("aux-func.R")

# Step 1 - Fit the models ======================================================
# Load data
source("data-zurich.R")

# TEST: surveillance time series object created which has the same length as
# our study period
library(tinytest)
source("tests/test-sts-creation.R")
rm(pop); rm(props); rm(zhpop)

# Load covariates ..............................................................
load("covariate_options.Rda")
# Run check that covariates created correctly
source("tests/test-covariate-creation.R")
#summary(test_results_covariates)
if(docu){
  cat(min(day), file = "document/day-min.tex")
  cat(max(day), file = "document/day-max.tex")
}
# Check correct number of simulated contact matrices
if(length(mat_array_list) != nsim){
  stop("Value of nsim changed, please re-run cont-mat.R")
}

# Fit models with Poisson lag ..................................................
if((check1 <- !file.exists("model_options_poisson.Rda"))){
  message("Models not run before -- running models")
  source("step1-fit-models.R")
}
if((check2 <- file.info("model_options_poisson.Rda")$mtime <
   file.info("step1-fit-models.R")$mtime)){
  message("Model fitting script changed -- rerunning models")
  source("step1-fit-models.R")
}
if((check3 <- file.info("model_options_poisson.Rda")$mtime <
   file.info("cont-mat.R")$mtime |
   file.info("model_options_poisson.Rda")$mtime <
   file.info("data/mat_array-zh.Rda")$mtime)){
  message("Contact matrices adjusted -- rerunning models")
  source("step1-fit-models.R")
}

# Examine model convergence ....................................................
mods <- list()
load("model_options_poisson.Rda")
mods$poisson <- model_options
rm(model_options)
# Run checks that models are created the way we want
source("tests/test-model-creation.R")

# Create overview of models with BIC etc .......................................
if(any(check1, check2, check3)){
  message("Creating overview of models")
  overview <- data.frame(
    Lag = c(rep("Poisson", length(mods$poisson))),
    Name = c(names(mods$poisson)),
    Description = c(sapply(seq_len(length(mods$poisson)), function(x){
      mods$poisson[[x]]$mod$model_description})),
     BIC = c(sapply(seq_len(length(mods$poisson)), function(x){
      ifelse(mods$poisson[[x]]$mod$convergence,
             BIC(mods$poisson[[x]]$mod),
             NA_real_)
    })),
    Parameters = c(sapply(lapply(seq_len(length(mods$poisson)), function(x){
      mods$poisson[[x]]$mod
    }), function(x){
      suppressWarnings(attr(logLik(x), "df"))
    }
    ))
  )
  # Put the results in a table
  copy <- overview
  overview$Name <- paste0("\\code{", overview$Name, "}")
  overview <- overview[overview$Lag == "Poisson", ]
  # Sort this table then save it ...............................................
  overview <- overview[order(overview$BIC), ]
  rownames(overview) <- NULL
  cat(kable(overview, booktabs = TRUE, linesep = "",
            escape = FALSE),
      file = "tables/models-overview.tex")
  # Create version where we have added the checkmarks denoting effects
  # included in the model
  ovrvw <-
    rbind(c("\\checkmark", NA, "\\checkmark", "\\checkmark", NA, "\\checkmark"),
          c("\\checkmark", NA, NA, "\\checkmark", NA, "\\checkmark"),
          c("\\checkmark", NA, "\\checkmark", "\\checkmark", NA, NA),
          c("\\checkmark", NA, NA, "\\checkmark", NA, NA),
          c("\\checkmark", NA, "\\checkmark", NA, NA, "\\checkmark"),
          c(NA, NA, "\\checkmark", "\\checkmark", NA, "\\checkmark"),
          c("\\checkmark", NA, NA, NA, NA, NA),
          c(NA, NA, "\\checkmark", NA, NA, NA),
          c(NA, NA, "\\checkmark", NA, NA, "\\checkmark"),
          c(NA, "\\checkmark", "\\checkmark", NA, "\\checkmark", NA),
          c(NA, "\\checkmark", "\\checkmark", NA, NA, "\\checkmark"),
          c(NA, "\\checkmark", "\\checkmark", NA, "\\checkmark", "\\checkmark"),
          c(NA, NA, "\\checkmark", NA, "\\checkmark", "\\checkmark"),
          c(NA, "\\checkmark", NA, NA, "\\checkmark", "\\checkmark"),
          c(NA, NA, NA, "\\checkmark", NA, NA),
          c(NA, NA, NA, NA, NA, "\\checkmark"),
          c(NA, "\\checkmark", NA, NA, "\\checkmark", NA),
          c(NA, NA, NA, NA, "\\checkmark", NA),
          c(NA, "\\checkmark", NA, NA, NA, NA),
          c(NA, NA, NA, NA, NA, NA))
  ovrvw <- cbind(c("zh_mod_SxL_SxL",
                   "zh_mod_Sxx_SxL",
                   "zh_mod_SxL_Sxx",
                   "zh_mod_Sxx_Sxx",
                   "zh_mod_SxL_xxL",
                   "zh_mod_xxL_SxL",
                   "zh_mod_Sxx_xxx",
                   "zh_mod_xxL_xxx",
                   "zh_mod_xxL_xxL",
                   "zh_mod_xTL_xTx",
                   "zh_mod_xTL_xxL",
                   "zh_mod_xTL_xTL",
                   "zh_mod_xxL_xTL",
                   "zh_mod_xTx_xTL",
                   "zh_mod_xxx_Sxx",
                   "zh_mod_xxx_xxL",
                   "zh_mod_xTx_xTx",
                   "zh_mod_xxx_xTx",
                   "zh_mod_xTx_xxx",
                   "zh_mod_xxx_xxx"),
                 ovrvw)
  ovrvw <- as.data.frame(ovrvw)
  names(ovrvw)[1] <- "Name"
  overview <- merge(ovrvw, copy[, c("Lag", "Name", "BIC", "Parameters")])
  # Provide column names
  colnames(overview) <- c("Name", rep(c("Seasonality", "Temperature", "Time"),
                                      times = 2),
                          "Lag", "BIC", "Parameters")
  # Sort table by BIC before saving
  overview <- overview[order(overview$BIC), ]
  rownames(overview) <- NULL
  overview$Name <- paste0("\\code{", overview$Name, "}")
  cat(add_header_above(kable(overview, booktabs = TRUE,
                             linesep = "",
                             escape = FALSE),
                       header = c("", "Endemic / $\\\\nu_{at}$" = 3,
                                  "Epidemic / $\\\\phi_{at}$" = 3,
                                  "", "", ""),
                       escape = FALSE),
      file = "tables/models-overview2.tex")
}

# Step 1b - Best fitting model -------------------------------------------------
if(any(check1, check2, check3) |
   !file.exists("best_model.Rda")){
  message("Finding best model by BIC")
  # Extract the best model from the overview table based on BIC
  best <- mods$poisson
  lookup <-
    data.frame(
      Name = c("zh_mod_xxx_xxx",
               "zh_mod_xxL_xxL",
               "zh_mod_xxx_xxL",
               "zh_mod_xxL_xxx",
               "zh_mod_Sxx_Sxx",
               "zh_mod_xxx_Sxx",
               "zh_mod_Sxx_xxx",
               "zh_mod_SxL_SxL",
               "zh_mod_SxL_xxL",
               "zh_mod_SxL_Sxx",
               "zh_mod_xxL_SxL",
               "zh_mod_Sxx_SxL",
               "zh_mod_xTx_xTx",
               "zh_mod_xxx_xTx",
               "zh_mod_xTx_xxx",
               "zh_mod_xTL_xTL",
               "zh_mod_xTL_xxL",
               "zh_mod_xTL_xTx",
               "zh_mod_xxL_xTL",
               "zh_mod_xTx_xTL"),
      Description = c(
        "No extra effects",
        "Time in endemic and epidemic",
        "Time in epidemic",
        "Time in endemic",
        "Seasonality in endemic and epidemic",
        "Seasonality in epidemic",
        "Seasonality in endemic",
        "Seasonality and time in endemic and epidemic",
        "Seasonality and time in endemic and time in epidemic",
        "Seasonality and time in endemic and seasonality in epidemic",
        "Time in endemic and seasonality and time in epidemic",
        "Seasonality in endemic and seasonality and time in epidemic",
        "Temperature in endemic and epidemic",
        "Temperature in epidemic",
        "Temperature in endemic",
        "Temperature and time in endemic and epidemic",
        "Temperature and time in endemic and time in epidemic",
        "Temperature and time in endemic and temperature in epidemic",
        "Time in endemic and temperature and time in epidemic",
        "Temperature in endemic and temperature and time in epidemic"))
  # Due to divergence issues, do not consider models with temperature
  overview <- copy
  overview <- overview[!grepl("emperature", overview$Description), ]
  best <- lapply(best, `[`, which(!grepl("emperature", overview$Description)))
  # Sort by BIC
  overview <- overview[order(overview$BIC), ]
  if(docu){
    cat(overview[1, ]$Parameters, file = "document/number-model-params.tex")
  }
  # Save copy of this
  cat(add_header_above(kable(overview, booktabs = TRUE,
                             linesep = "",
                             escape = FALSE),
                       header = c("", "Endemic / $\\\\nu_{at}$" = 3,
                                  "Epidemic / $\\\\phi_{at}$" = 3,
                                  "", "", ""),
                       escape = FALSE),
      file = "tables/models-overview3.tex")
  desc_to_match <- lookup[lookup$Name ==
                            gsub("\\}", "",
                                 gsub("\\\\code\\{", "",
                                      overview[1, ]$Name)), ]$Description
  select <- which(sapply(1 : dim(lookup)[1],
                         function(x){
                           best[[x]]$mod$model_description == desc_to_match
                  }))
  zh_mod <- best[[select]]$mod
  # Check
  run_test("Best model chosen -- BIC", {
    for(i in seq_len(length(best))){
      if(i == select){
        # next
        expect_true_wrapper(BIC(zh_mod) == BIC(best[[i]]$mod))
      } else {
        # check whether model converged (otherwise BIC() does not work)
        converged <- ifelse(best[[i]]$mod$convergence, TRUE, FALSE)
        if(converged){
          expect_true_wrapper(BIC(zh_mod) <= BIC(best[[i]]$mod))
        }
      }
    }
  })
  # Save the model selected by the model selection process
  save(zh_mod, file = "best_model.Rda")
  rm(desc_to_match)
}
load(file = "best_model.Rda")

# Plot lag distributions of all models
pdf("figures/lag-all.pdf", width = 10, height = 7.5)
default <- par(no.readonly = TRUE)
# this only stores those parameters that actually can be changed
# avoids warning later on)
par(mar = default$mar + c(12, 0, 0, 0))
plot(c(sapply(seq_len(length(mods$poisson)), function(x){
  mods$poisson[[x]]$misc$highest_val})), xaxt = "n", xlab = "",
  ylab = "Lag distribution peak", ylim = c(1, 7),
  main = "Discrete time serial intervals",
  pch = ifelse(sapply(seq_len(length(mods$poisson)), function(x){
    mods$poisson[[x]]$mod$model_description}) ==
      zh_mod$model_description, 19, 1))
axis(1, at = seq_len(length(mods$poisson)),
     labels = FALSE) # Add tick labels
text(x = seq_len(length(mods$poisson)),
     y = par("usr")[3] - 0.5,
     labels = c(sapply(seq_len(length(mods$poisson)), function(x){
       mods$poisson[[x]]$mod$model_description})),
     xpd = NA,
     adj = 0.5, # Center labels on tick marks
     srt = 45, # Rotate
     pos = 2, # Right-justify
     cex = 0.8) # Label size
par(default)
invisible(dev.off())

# Plot the lag distribution and fit of this model
pdf("figures/lag.pdf", width = 4, height = 3)
(p <- plot_lag(zh_mod, titl = "") +
  scale_x_continuous(labels = 1 : 7, breaks = 1 : 7))
invisible(dev.off())
pdf("figures/base-model.pdf", width = 8, height = 4)
plotHHH4lag_fitted(zh_mod, xaxis = myaxis,
                   units = NULL,
                   pch = 20,
                   hide0s = FALSE,
                   col = rep(palette()[3], 3),
                   legend = FALSE,
                   names = labls, ylab = "COVID-19 cases", pt.cex = 0.3)
invisible(dev.off())

# Extract the model coefficients
model_tab <- coef_table(zh_mod,
                        side_by_side = TRUE)
colnames(model_tab) <-
  rep(c("Coefficient", "Estimate", "Standard error"), 3)
head <- dim(model_tab)[2]
names(head) <- zh_mod$model_description
cat(#add_header_above(
  kable(model_tab,
        booktabs = TRUE, linesep = "",
        escape = FALSE),
  #                   header = head),
  file = "tables/models-tab.tex")

# Step 2 - Prediction of counterfactual scenarios ------------------------------
if(isTRUE(any(check1, check2, check3))){
  message("Running prediction step for best fitting model")
  # Save copies of model for school closure scenarios
  zh_modA <- zh_modB <- zh_mod
  set.seed(13)
  # Replacement
  zh_modB$control$ne$weights <- sc_mat_array
  zh_modB$terms <- NULL # Fix from JB
  test_results_setup_pred <- run_test_file("tests/predictions/test-creation.R",
                                           verbose = 0)
  summary(test_results_setup_pred)
  # Prediction of hhh4lag objects
  mom_A <- predictive_moments(zh_modA,
                              t_condition = sta_tm,
                              lgt = dim(cases)[1] - sta_tm,
                              return_Sigma = TRUE,
                              return_cov_array = TRUE,
                              return_mu_decomposed = TRUE)
  mom_B <- predictive_moments(zh_modB,
                              t_condition = sta_tm,
                              lgt = dim(cases)[1] - sta_tm,
                              return_Sigma = TRUE,
                              return_cov_array = TRUE,
                              return_mu_decomposed = TRUE)
  # Check for missings - shouldn't be the case
  if(isTRUE(dimnames(table(is.na(sc_mat_array)))[[1]] == TRUE) |
     isTRUE(dimnames(table(is.na(mom_B)))[[1]] == TRUE)){
    warning("Check sc_mat_array and mom_B")
    stop("Predicted cases will be NA")
  }
  test_results_success_pred <-
    run_test_file("tests/predictions/test-creation-pt2.R", verbose = 0)
  summary(test_results_success_pred)
  # Plot the trajectory compared to the baseline scenario
  df <- mom_B$mu_matrix / mom_A$mu_matrix
  df <- as.data.frame(df)
  df$date <- case_dates[(sta_tm + 1) : dim(cases)[1]]
  df$date <- as.Date(df$date)
  pdf("figures/patterns.pdf", width = 6, height = 4)
  p1 <- ggplot() +
    geom_line(data = df, aes(x = date, y = `0-14`, colour = "0-14")) +
    geom_line(data = df, aes(x = date, y = `15-24`, colour = "15-24")) +
    geom_line(data = df, aes(x = date, y = `25-44`, colour = "25-44")) +
    geom_line(data = df, aes(x = date, y = `45-65`, colour = "45-65")) +
    geom_line(data = df, aes(x = date, y = `66-79`, colour = "66-79")) +
    geom_line(data = df, aes(x = date, y = `80+`, colour = "80+")) +
    labs(y = "Increase in COVID-19 cases",
         x = "", colour = "Age group")
  print(p1 +
    theme(legend.position = "bottom") +
    coord_trans(y = "log10"))
  invisible(dev.off())
  print(paste("Created: figures/patterns.pdf"))
}

if(isTRUE(any(check1, check2, check3)) |
   # If file doesn't already exist
   !file.exists("data/coef-list.Rda") |
   # If code has changed
   file.info("analysis.R")$mtime >
   file.info("data/coef-list.Rda")$mtime){
  message("Saving model coefficients for incorporating of internal uncertainty")
  # Step 2b - Incorporate uncertainties ====
  coef_list <- list()
  for(i in seq_len(nsim)){
    set.seed(i) # Reproducibility
    newcoef <- mvrnorm(n = 1,
                       mu = zh_mod$coefficients,
                       Sigma = zh_mod$cov)
    coef_list[[i]] <- newcoef
  }
  save(coef_list, file = "data/coef-list.Rda")
  pdf(paste0("figures/", suffix, "coefficients-simulated.pdf"),
      width = 2, height = 4)
  tmp <- do.call(rbind, coef_list)
  colnames(tmp) <- change_labels(colnames(tmp))
  sapply(seq_len(dim(tmp)[2]),
         function(x){
           boxplot(tmp[, x, drop = FALSE],
                             main = latex2exp::TeX(sprintf(colnames(tmp)[x])))
           })
  invisible(dev.off())
}
load("data/coef-list.Rda")

if(!file.exists("data/mom_array-zh-preds.Rda") |
                file.info("model_options_poisson.Rda")$ctime >=
   file.info("data/mom_array-zh-preds.Rda")$ctime){
  # For each simulated coefficients and contact matrix
  # do the prediction
  message("Predict cases including simulated weights and coefficients")
  mom_array_A_mean <-
    mom_array_B_mean <- array(NA_real_,
                              dim = c(dim(cases)[1] - sta_tm,
                                      # Length of prediction window
                                      dim(cases)[2], # Number of age groups
                                      nsim), # Number of simulations
                              dimnames = list(
                                date = tail(rownames(cases),
                                            dim(cases)[1] - sta_tm),
                                age_group = labls,
                                simulation = seq_len(nsim)))
  # If yes, create new ones
  i <- 1
  while(i <= nsim){
    start_time <- Sys.time()
    load(file = "best_model.Rda")
    # Save copies of model for school closure scenarios
    zh_modA <- zh_modB <- zh_mod
    # Prepare the two scenarios
    zh_modA$control$ne$weights <- mat_array_list[[i]]
    zh_modA$coefficients <- # A and B are the same here
      zh_modB$coefficients <- coef_list[[i]]
    zh_modA$terms <- NULL # Fix from JB
    # Replacement
    zh_modB$control$ne$weights <- sc_mat_array_list[[i]]
    zh_modB$terms <- NULL # Fix from JB
    # Note that A and B are now different
    # Prediction of hhh4lag objects
    mom_A <- predictive_moments(zh_modA,
                                t_condition = sta_tm,
                                lgt = dim(cases)[1] - sta_tm,
                                return_Sigma = FALSE,
                                # Increases speed slightly
                                return_cov_array = TRUE)
    mom_B <- predictive_moments(zh_modB,
                                t_condition = sta_tm,
                                lgt = dim(cases)[1] - sta_tm,
                                return_Sigma = FALSE,
                                # Increases speed slightly
                                return_cov_array = TRUE)
    # Sanity check
    if(isTRUE(dimnames(table(is.na(sc_mat_array_list[[i]])))[[1]] == TRUE) |
       isTRUE(dimnames(table(is.na(mom_B)))[[1]] == TRUE)){
      warning("Check sc_mat_array and mom_B")
      stop("Predicted cases will be NA")
    }
    if(isTRUE(any(is.nan(mom_B$mu_matrix)))){
      warning(paste(
        "Check mom_B - giving NaN (invalid calcuation) for prediction", i))
    }
    # Relabel
    rownames(mom_A$mu_matrix) <-
      rownames(cases)[as.numeric(gsub("t=", "",
                                      rownames(mom_A$mu_matrix)))]
    rownames(mom_B$mu_matrix) <-
      rownames(cases)[as.numeric(gsub("t=", "",
                                      rownames(mom_B$mu_matrix)))]
    # Save the predicted counts
    for(j in seq_len(dim(mom_A$mu_matrix)[1])){
      mom_array_A_mean[j, , i] <- mom_A$mu_matrix[j, ]
    }
    for(j in seq_len(dim(mom_B$mu_matrix)[1])){
      mom_array_B_mean[j, , i] <- mom_B$mu_matrix[j, ]
    }
    end_time <- Sys.time()
    print(paste("Prediction", i, "took:", end_time - start_time))
    i <- i + 1
  }
  save(mom_array_A_mean, mom_array_B_mean,
       file = "data/mom_array-zh-preds.Rda")
}
load("data/mom_array-zh-preds.Rda")

# Check if any of the predictions to not provide a full trajectory
# Should not happen
test_results_simulation_A <-
  run_test_file("tests/simulations/test-simulations-A.R",
                verbose = 0)
test_results_simulation_B <-
  run_test_file("tests/simulations/test-simulations-B.R",
                verbose = 0)
test_results_simulation <-
  run_test_file("tests/simulations/test-simulations-A-greater-than-B.R",
                verbose = 0)
summary(test_results_simulation_A)
summary(test_results_simulation_B)
summary(test_results_simulation)

perc <- list()
i <- 1
while(i <= nsim){
    # Case counts and scenario comparison
    perc[[i]] <-
      combine_predicted_counts(pred_A = mom_array_A_mean[, , i],
                               pred_B = mom_array_B_mean[, , i])$total_counts
  i <- i + 1
}

# Specific format of table desired
res <- summarise_predicted_counts(perc); res_f <- res
for (i in 2 : 7){
  res_f[, i] <- formatC(res_f[, i], digits = 0, format = "f")
}
for (i in 8 : 10){
  res_f[, i] <- formatC(res_f[, i], digits = 1, format = "f")
}
for (i in 11 : 13){
  res_f[, i] <- formatC(res_f[, i], digits = 2, format = "f")
}

# Save
cat(add_header_above(kable(res_f,
                           booktabs = TRUE,
                           escape = FALSE,
                           linesep = ""), # vector with colspan
                     header = c("",
                                "Scenario A" = 3,
                                "Scenario B" = 3,
                                "B - A" = 3,
                                "B / A" = 3)),
    file = "tables/perc-simul.tex")
rm(res_f)

if(docu){
  cat(round(res[res$Age == "0-14",
                max(which(colnames(res) == "Median"))] * 100 - 100),
      file = "document/youngest-increase.tex")
  cat(round(res[res$Age == "15-24",
                max(which(colnames(res) == "Median"))] * 100 - 100),
      file = "document/secondyoungest-increase.tex")
  cat(round(res[res$Age == "80+",
                max(which(colnames(res) == "Median"))] * 100 - 100),
      file = "document/oldest-increase.tex")
  cat(round(res[res$Age == "Total (summed)",
                max(which(colnames(res) == "Median"))] * 100 - 100),
      file = "document/total-increase.tex")
  cat(round(max(res[, max(which(colnames(res) == "Median"))] * 100 - 100)),
      file = "document/max-increase.tex")
  cat(round(res[res$Age == "80+", which(colnames(res) == "Median")[3]]),
      file = "document/oldest-increase-counts.tex")
  cat(round(res[res$Age == "Total (summed)",
                which(colnames(res) == "Median")[3]]),
      file = "document/total-increase-counts.tex")
  cat(round(res[res$Age == "80+",
                which(colnames(res) == "P\\textsubscript{90}")[3]]),
      file = "document/oldest-increase-counts90.tex")
  cat(round(res[res$Age == "Total (summed)",
                which(colnames(res) == "P\\textsubscript{90}")[3]]),
      file = "document/total-increase-counts90.tex")
  save(res, file = "document/orig-pred.Rda")
}

rm(res); rm(perc); rm(tgthr)
rm(head); rm(model_tab)
rm(tmp); rm(tmp2); rm(tmp3)
rm(overview); rm(res2)
rm(best)
rm(zh_mod); rm(zh_modA); rm(zh_modB)
rm(i)
