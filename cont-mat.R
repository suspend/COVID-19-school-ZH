# Run setup
source("setup.R")
source("aux-func.R")

# Define this function here because it is needed for tests
if(flip){
  aggregate_matrix <- function(cont){
    aggregateC(cont, weights = pop_weight_frac, grouping = grouping)
  }
} else {
  aggregate_matrix <- function(cont){
    aggregateC(t(cont), weights = pop_weight_frac, grouping = grouping)
  }
}

# Load contact data ------------------------------------------------------------
if(!file.exists("data/contact-matrices.Rda")){
  message("Creating data/contact-matrices.Rda")
  # Use Mistry et al. (2021)
  if(isFALSE(file.exists(
    "data/Switzerland_country_level_F_community_setting_85.csv"))){
    download.file("https://github.com/mobs-lab/mixing-patterns/raw/main/data/contact_matrices/Switzerland_country_level_F_community_setting_85.csv",
                  destfile =
                    "data/Switzerland_country_level_F_community_setting_85.csv")
  }
  if(isFALSE(file.exists(
    "data/Switzerland_country_level_F_household_setting_85.csv"))){
    download.file("https://github.com/mobs-lab/mixing-patterns/raw/main/data/contact_matrices/Switzerland_country_level_F_household_setting_85.csv",
                  destfile =
                    "data/Switzerland_country_level_F_household_setting_85.csv")
  }
  if(isFALSE(file.exists(
    "data/Switzerland_country_level_F_school_setting_85.csv"))){
    download.file("https://github.com/mobs-lab/mixing-patterns/raw/main/data/contact_matrices/Switzerland_country_level_F_school_setting_85.csv",
                  destfile =
                    "data/Switzerland_country_level_F_school_setting_85.csv")
  }
  if(isFALSE(file.exists(
    "data/Switzerland_country_level_F_work_setting_85.csv"))){
    download.file("https://github.com/mobs-lab/mixing-patterns/raw/main/data/contact_matrices/Switzerland_country_level_F_work_setting_85.csv",
                  destfile =
                  "data/Switzerland_country_level_F_work_setting_85.csv")
  }
  # added "header = FALSE" as on R 4.1.0 the default is TRUE
  # and this interprets the first row as column names.
  school <- read.csv("data/Switzerland_country_level_F_school_setting_85.csv",
                     header = FALSE)
  work <- read.csv("data/Switzerland_country_level_F_work_setting_85.csv",
                   header = FALSE)
  household <- read.csv(
    "data/Switzerland_country_level_F_household_setting_85.csv",
                        header = FALSE)
  other <- read.csv("data/Switzerland_country_level_F_community_setting_85.csv",
                    header = FALSE)

  m_household <- as.matrix(household)
  m_school <- as.matrix(school)
  m_work <- as.matrix(work)
  m_other <- as.matrix(other)

  # This creates the labels for the contact matrices such that it is clear what
  # age group the corresponding row/column represents.
  # The ifelse statement ensures that the labels follow the convention that the
  # minimum and maximum age have two digits and follow the format xx-xx
  ## Create list of numbers sequentially from 00 to 84
  row_col_names <- ifelse(nchar(0 : (ncol(m_other) - 1)) == 1,
                          paste0(0, 0 : (ncol(m_other) - 1)),
                          (0 : (ncol(m_other) - 1)))
  ## Paste with copy of itself plus one and add dash between the two
  row_col_names <- paste0(row_col_names, "-",
                          ifelse(nchar(as.numeric(row_col_names) + 1) == 1,
                                 # if one digit add leading zero
                                 paste0("0", as.numeric(row_col_names) + 1),
                                 as.numeric(row_col_names) + 1))
  ## Add plus at very end
  row_col_names[length(row_col_names)] <-
    gsub("-.+", "\\+", row_col_names[length(row_col_names)])

  # Note the difference between how contact matrices usually shown (POLYMOD) and
  # how they enter endemic-epidemic models (Meyer and Held 2017)
  if(flip){
    dim_names <- list(participant = row_col_names, contact = row_col_names)
  } else {
    dim_names <- list(contact = row_col_names, #columns
                      participant = row_col_names # rows
    )
  }

  # The dimnames are inherited by other contact matrix-related functions we use
  # Which is why we set them
  dimnames(m_household) <- dim_names
  dimnames(m_work) <- dim_names
  dimnames(m_school) <- dim_names
  dimnames(m_other) <- dim_names
  save(m_work, m_household, m_school, m_other,
       file = "data/contact-matrices.Rda")
}
load(file = "data/contact-matrices.Rda")
dm <- dim(m_household)[1]
# check whether all have the correct dimension
cond <- all(c(dim(m_work), dim(m_household), dim(m_school), dim(m_other)) == dm)
if(!cond){
  stop("You are missing parts of the contact matrix")
}

# Load Mistry et al. weights ---------------------------------------------------
if(!file.exists("data/cont-weights.Rda")){
  message("Creating data/cont-weights.Rda")
  # Disease weights
  # Define the disease weights as given in the Mistry et al. manuscript
  mistry <- c("school" = 11.41, "work" = 8.07,
              "household" = 4.11, "other" = 2.79)
  se_mistry <- c("school" = 0.27, "work" = 0.52,
                 "household" = 0.41, "other" = 0.48)
  if(docu){
    cat(paste0("$w^{\\text{school}} = ", mistry["school"],
               "$ (standard error (SE) ", se_mistry["school"],
               "), $w^{\\text{work}} = ", mistry["work"],
               "$ (SE ", se_mistry["work"],
               "), $w^{\\text{household}} = ", mistry["household"],
               "$ (SE ", se_mistry["household"],
               "), and $w^{\\text{other}} = ", mistry["other"],
               "$ (SE ", se_mistry["other"], ")"),
        file = "document/mistry.tex")
  }
  # Simulate the disease weights from a normal distribution
  sim_mistry <- matrix(NA, nrow = nsim, ncol = 4)
  for(i in seq_len(nsim)){
    set.seed(i) # Reproducibility
    newcontacts <- rnorm(4, mean = mistry, sd = se_mistry)
    sim_mistry[i, ] <- newcontacts
  }
  colnames(sim_mistry) <- names(mistry)
  # Save the object
  save(mistry, se_mistry, sim_mistry, file = "data/cont-weights.Rda")
}
load("data/cont-weights.Rda")

pdf(paste0("figures/", suffix, "simul-weights.pdf"),
    width = 8, height = 6)
par(las = 1)
boxplot(sim_mistry, names = colnames(sim_mistry))
invisible(dev.off())

# Load population data ---------------------------------------------------------
if(!file.exists("data/population.Rda")){
  message("Creating data/population.Rda")
  # Use https://www.zh.ch/de/soziales/bevoelkerungszahlen.html?keyword=bevoelkerung#/details/254@statistisches-amt-kanton-zuerich
  if(isFALSE(file.exists(
    "data/KANTON_ZUERICH_bevoelkerung_1jahresklassen.csv"))){
    download.file("https://www.web.statistik.zh.ch/ogd/data/KANTON_ZUERICH_bevoelkerung_1jahresklassen.csv",
                  destfile =
                    "data/KANTON_ZUERICH_bevoelkerung_1jahresklassen.csv")
  }
  zhpop <- read.csv("data/KANTON_ZUERICH_bevoelkerung_1jahresklassen.csv",
                    sep = ";", header = TRUE, fileEncoding = "UTF-8-BOM")
  # Select data from the most recent year
  zhpop <- zhpop[zhpop$JAHR == max(zhpop$JAHR), ]
  # Get population by age group
  pop <- aggregate(ANZAHL_PERSONEN ~ ALTERSKLASSE_CODE, zhpop, sum)
  # Replace oldest category (determined by contact matrix limits) with all
  # population members of same age and above
  pop$ANZAHL_PERSONEN[dm] <-
    sum(pop$ANZAHL_PERSONEN[seq(from = dm, to = nrow(pop), by = 1)])
  # Reduce population data to same dimension as contacts
  pop <- pop[seq_len(dm), ]
  # Calculate population fractions
  pop$frac <- pop$ANZAHL_PERSONEN / sum(pop$ANZAHL_PERSONEN)
  # "include.lowest = TRUE" as otherwise gives AGEGRP = NA for age 0
  # Group by the age groups we are interested in
  zhpop$AGEGRP <- cut(as.numeric(zhpop$ALTERSKLASSE_CODE),
                      breaks = brk,
                      include.lowest = TRUE)
  pop_tab <- aggregate(ANZAHL_PERSONEN ~ AGEGRP, zhpop, sum)
  pop_tab$AGEGRP <- labls
  colnames(pop_tab) <- c("Age group", "Size")
  if(docu){
    cat(kable(pop_tab,
              booktabs = TRUE, linesep = "",
              escape = FALSE, format = "latex"),
        file = "tables/population.tex")
  }
  save(zhpop, pop, pop_tab, file = "data/population.Rda")
}
load(file = "data/population.Rda")

# Run checks that matrices have been created correctly
source("tests/test-contact-matrix-creation.R")
# Run checks that population data has been created correctly
source("tests/test-population-creation.R")

# Create weights for use with hhh4contacts aggregation
pop_weight_frac <- pop$frac
pop_weight_count <- pop$ANZAHL_PERSONEN
names(pop_weight_frac) <-
  names(pop_weight_count) <-
  dimnames(m_household)$participant

# expand all these matrices to arrays of length of the study period
# This code just copies the matrices 161 times and puts them in an array
dimnames <- append(dimnames(m_household), list(day = as.character(case_dates)))
array_household <- array(rep(m_household, length(case_dates)),
                         dim = c(dim(m_household), length(case_dates)),
                         dimnames = dimnames)
array_work <- array(rep(m_work, length(case_dates)),
                    dim = dim(array_household),
                    dimnames = dimnames)
array_no_school <- array(rep(m_school, length(case_dates)),
                         dim = dim(array_household),
                         dimnames = dimnames)
array_other <- array(rep(m_other, length(case_dates)),
                     dim = dim(array_household),
                     dimnames = dimnames)
source("tests/test-contact-matrix-array-creation.R")

# Grouping required for aggregating matrices
# Determines the size of the age groups considered
# This is needed for the preprocessing of the school matrix
# (incorporating school holidays)
# as well as the aggregation of contact matrices to age groups
grouping <- vapply(labls, function(x){
  # get start and end age of the group based on the location of the dash
  # in the label (which is defined based on the object brk from setup.R)
  start_end <- unlist(strsplit(x, "-"))
  if(length(start_end) == 1){
    start_end <- c(start_end, dm - 1)
  }
  # convert to numeric
  start_end <- as.numeric(gsub("\\D", "", start_end))
  # return length of the sequence from start to end
  return(length(seq(start_end[1], start_end[2], by = 1)))
}, numeric(1L), USE.NAMES = FALSE)
# Run check
source("tests/test-grouping.R")

# Load policy data -------------------------------------------------------------
if(!file.exists("data/policy.Rda")){
  message("Creating data/policy.Rda data set")
  source("zh_indicators.R")
}
load("data/policy.Rda")
policy <- policy[policy$date %in% case_dates, ]
# Households never reduced
policy$C6_household <- 1
# Not reduced fully to reflect essential workers and teachers
policy$C2_work <- ifelse(policy$C2_work == 0, 0.1, policy$C2_work)
# Not reduced fully to reflect shopping etc.
policy$C4_other <- ifelse(policy$C4_other == 0, 0.1, policy$C4_other)
# Run check
source("tests/test-policy-creation.R")

# Load school holidays ---------------------------------------------------------
if(!file.exists("data/school-hols.Rda")){
  message("Creating data/school-hols.Rda data set")
  source("publ-hol.R")
}
load("data/school-hols.Rda")
# Contact levels are 0 if it is a school holiday
scl <- abs((1 * (case_dates %in% sch$date)) - 1)
# Run check
source("tests/test-school-holiday-creation.R")

# Use slight offsets/jitter in visualisaion
pdf(paste0("figures/", suffix, "measure_zh_all.pdf"),
    width = 6, height = 4)
ggplot(data = policy) +
  geom_line(aes(x = date, y = C1_school + 0.01,
                colour = "School closure status")) +
  geom_line(aes(x = date, y = C2_work + 0.005,
                colour = "Remote work status")) +
  geom_line(aes(x = date, y = C4_other - 0.005,
                colour = "Restrictions on gatherings (other)")) +
  labs(y = "Policy indicator",
       x = "", colour = "Measure") +
  theme(legend.position = "bottom")
invisible(dev.off())

# Sequence of contact matrices to be plotted to show changes over time
# Starting with first day which is not a school holiday as else change
# might not be as apparant to reader
inx <- floor(seq(which(scl == 1)[1], length(case_dates), length.out = 10))
# This is used as an example to illustrate
scl[inx[5]] # 1 so not a school holiday -- contact levels remain as they were
# Show specific date for illustration purposes
pdf(paste0("figures/", suffix, "construction-illustration1.pdf"),
    width = 6, height = 4)
ggplot(data = policy) +
  geom_line(aes(x = date, y = C1_school + 0.01,
                colour = "School closure status")) +
  geom_line(aes(x = date, y = C2_work + 0.005,
                colour = "Remote work status")) +
  geom_line(aes(x = date, y = C4_other - 0.005,
                colour = "Restrictions on gatherings (other)")) +
  geom_point(data = policy[policy$date == case_dates[inx[5]], ],
             aes(x = date, y = C1_school + 0.01), size = 1) +
  geom_point(data = policy[policy$date == case_dates[inx[5]], ],
             aes(x = date, y = C2_work + 0.005), size = 1) +
  geom_point(data = policy[policy$date == case_dates[inx[5]], ],
             aes(x = date, y = C4_other - 0.005), size = 1) +
  annotate("text", x = policy[policy$date == case_dates[inx[5]], ]$date,
           y = policy[policy$date == case_dates[inx[5]], ]$C2_work + 0.04,
           label = case_dates[inx[5]]) +
  labs(y = "Policy indicator",
       x = "", colour = "Measure") +
  theme(legend.position = "bottom")
invisible(dev.off())

if(!file.exists("data/mat_array-zh.Rda") |
    file.info("cont-mat.R")$mtime >
    file.info("data/mat_array-zh.Rda")$mtime){
  if(!file.exists("data/mat_array-zh.Rda")){
    message("data/mat_array-zh.Rda. does not exist -- creating")
  } else {
    message("Contact matrix creation script changed -- rerunning matrices")
  }
  # Create the time-varying contact matrices including the indicators from above
  # Incorporate school holidays closures before applying policy
  # such that it applies to both scenarios/sets of contact matrices
  before_hols <- array_no_school
  for(i in seq(dim(array_no_school)[3])){
    array_no_school[ , , i] <- array_no_school[ , , i] * scl[i]
  }
  # test whether school holidays have been properly implemented
  source("tests/test-school-holiday-implementation.R")

  # Naming: array_school is used for scenario B - schools open
  # array_no_school is used for the "as is" scenario (A)
  # Apply policy indicators to contact matrix components
  for(i in seq(dim(array_no_school)[3])){
    array_no_school[ , , i] <- array_no_school[ , , i] * policy$C1_school[i]
  }
  for(i in seq(dim(array_work)[3])){
    array_work[ , , i] <- array_work[ , , i] * policy$C2_work[i]
  }
  for(i in seq(dim(array_other)[3])){
    array_other[ , , i] <- array_other[ , , i] * policy$C4_other[i]
  }
  # Save this for the scenario without school closures
  array_school <- array_no_school

  # Open schools for school aged children (youngest age group) ONLY
  for(i in seq(dim(array_school)[3])){
    for(j in seq_len(grouping[1])){
      array_school[ , j, i] <- m_school[ , j]
    }
  }
  for(i in seq(dim(array_school)[3])){
    for(j in seq_len(grouping[1])){
      array_school[j, , i] <- m_school[j, ]
    }
  }
  # School holidays still relevant type of closure
  for(i in seq(dim(array_no_school)[3])){
    for(j in seq_len(grouping[1])){
      array_school[ , j, i] <- array_school[ , j, i] * scl[i]
    }
  }
  # test whether school holidays are reflected array_school
  source("tests/test-array_school-for-correct-school-holidays.R")
  # Sanity check
  run_test("Scenario B has more contact opportunities than scenario A/baseline", {
    expect_true_wrapper(sum(array_school >= array_no_school) ==
                          prod(dim(array_no_school)))
    expect_equal_wrapper(sum(array_school >= array_no_school),
                         prod(dim(array_no_school)))
  })
  # Combine with the weights (linear combinations) and aggregate
  ## Create an array of desired dimensions which is NA
  mat_array <- array(NA_real_,
                     dim = c(length(grouping), length(grouping),
                             length(case_dates)),
                     dimnames = list(participant = labls, contact = labls,
                                     day = as.character(case_dates)))
  # Populate the array with the values
  for(i in seq(dim(array_household)[3])){
    # combine matrices
    cont <- mistry["school"] * array_no_school[ , , i] +
      mistry["work"] * array_work[ , , i]  +
      mistry["household"] * array_household[ , , i] +
      mistry["other"] * array_other[ , , i]
    # Aggregate (consider that aggregateC wants the matrix to be in
    # a specific format)
    mat_array[ , , i] <- aggregate_matrix(cont)
  }
  ### array for when the schools are open ---- SCENARIO B
  sc_mat_array <- array(NA_real_,
                        dim = c(length(grouping), length(grouping),
                                length(case_dates)),
                        dimnames = list(participant = labls, contact = labls,
                                        day = as.character(case_dates)))
  # Populate
  for(i in seq(dim(array_household)[3])){
    cont <- mistry["household"] * array_household[ , , i] +
      mistry["work"] * array_work[ , , i]  +
      mistry["school"] * array_school[ , , i] +
      mistry["other"] * array_other[ , , i]
    # Aggregate (NB aggregateC wants the matrix to be in a specific format)
    sc_mat_array[ , , i] <- aggregate_matrix(cont)
  }
  # Run checks
  source("tests/test-time-varying-contact-creation.R")
  # Plot matrices (this needs mat_array to work, so it was moved here)
  source("plot-cont-mat.R")
  # Save for use in analysis
  save(mat_array, sc_mat_array, file = "data/mat_array-zh.Rda")
}
load("data/mat_array-zh.Rda")

pop_weight_count_grp <- aggregate(ANZAHL_PERSONEN ~ AGEGRP,
                                  zhpop, sum)$ANZAHL_PERSONEN
names(pop_weight_count_grp) <- labls

tinytest::expect_equal(target = sum(pop_weight_count),
                       current = sum(pop_weight_count_grp),
                       info = "Sanity check of population counts")

# Create the lists with the simulated weights for the simulation
if(!file.exists("data/mat_array-zh-simuls.Rda") |
   file.info("cont-mat.R")$mtime >
   file.info("data/mat_array-zh-simuls.Rda")$mtime){
  if(!file.exists("data/mat_array-zh-simuls.Rda")){
    message("data/mat_array-zh-simuls.Rda. does not exist -- creating")
  } else {
    message("Contact matrix creation script changed -- rerunning matrices")
  }
  # Scenario A
  mat_array_list <- lapply(seq_len(nsim), function(x){
    tmp_array <- array(NA_real_, dim = c(length(grouping), length(grouping),
                                         length(case_dates)),
                       dimnames = list(participant = labls, contact = labls,
                                       day = as.character(case_dates)))
    for(i in seq_len(length(case_dates))){
      cont <- array_no_school[ , , i] * sim_mistry[x, "school"] +
        array_work[ , , i] * sim_mistry[x, "work"] +
        array_household[ , , i] * sim_mistry[x, "household"] +
        array_other[ , , i] * sim_mistry[x, "other"]
      # aggregate
      tmp_array[ , , i] <- aggregate_matrix(cont)
    }
    return(tmp_array)
  })
  # Scenario B
  sc_mat_array_list <- lapply(seq_len(nsim), function(x){
    tmp_array <- array(NA_real_, dim = c(length(grouping), length(grouping),
                                         length(case_dates)),
                       dimnames = list(participant = labls, contact = labls,
                                       day = as.character(case_dates)))
  for(i in seq_len(length(case_dates))){
    cont <- array_school[ , , i] * sim_mistry[x, "school"] +
      array_work[ , , i] * sim_mistry[x, "work"] +
      array_household[ , , i] * sim_mistry[x, "household"] +
      array_other[ , , i] * sim_mistry[x, "other"]
    # aggregate
    tmp_array[ , , i] <- aggregate_matrix(cont)
  }
    return(tmp_array)
  })
  # Run checks
  source("tests/test-time-varying-simul-creation.R")
  # Save simulated contact matrices
  save(mat_array_list, sc_mat_array_list,
       file = "data/mat_array-zh-simuls.Rda")
}
load("data/mat_array-zh-simuls.Rda")

# Clean up
rm(m_household); rm(m_other); rm(m_school); rm(m_work)
rm(policy)
rm(sch); rm(mat_array_list)
rm(sc_mat_array); rm(sc_mat_array_list); rm(sim_mistry)
rm(inx); rm(mat_array); rm(mistry);
rm(scl); rm(se_mistry)
