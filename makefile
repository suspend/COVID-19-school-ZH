default: all

# define variables (stuff that is produced by the same script)
temperature = data/temperature.Rda
policy-indicators = data/policy.Rda
holidays = data/school-hols.Rda data/hols.Rda
contact-matrices = data/population.Rda data/mat_array-zh.Rda data/no-school_mat_array-zh.Rda data/mat_array-zh-simuls.Rda data/no-school_mat_array-zh-simuls.Rda
analysis = figures/temperature-zh.pdf figures/test-rate.pdf covariate_options.Rda tables/models-overview2.tex best_model.Rda figures/lag.pdf figures/base-model.pdf tables/models-tab.tex figures/patterns.pdf data/coef-list.Rda tables/perc-simul.tex
sensitivity-analysis = figures/lag-comparison.pdf figures/sensitivity-lag.pdf tables/perc-simul-sens-g.tex tables/perc-simul-sens-s.tex

# define rules for preprocessing
$(temperature): setup.R
	Rscript temperature.R

$(policy-indicators): setup.R
	Rscript zh_indicators.R

$(holidays):
	Rscript publ-hol.R

load-required-data: $(temperature) $(policy-indicators) $(holidays)

# define rules for analysis
$(contact-matrices): setup.R aux-func.R $(policy-indicators) $(holidays)
	Rscript cont-mat.R

$(analysis): setup.R aux-func.R data-zurich.R $(holidays) $(temperature) $(contact-matrices) data/Dashboard_3_COVID19_labtests_positivity.csv
	Rscript analysis.R

$(sensitivity-analysis): setup.R data-zurich.R $(contact-matrices) $(analysis)
	Rscript sensitivity-lag.R

do-analysis: $(contact-matrices) $(analysis) $(sensitivity-analysis)

# define rules for output
$(document-results): zurich_analysis.tex references.bib referenceszh.bib analysis
	xelatex zurich-analysis; bibtex zurich-analysis; xelatex zurich-analysis; xelatex zurich-analysis

all: load-required-data do-analysis
