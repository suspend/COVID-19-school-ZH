# Overview

This repository contains code to conduct the analysis in the manuscript
_Assessing the effect of school closures on the spread of COVID-19 in Zurich_

## General code

The following scripts are used across the entire project

* [setup.R](setup.R) Settings and packages used
* [aux-func.R](aux-func.R) Auxiliary functions

## Pre-processing

The following scripts are used for pre-processing data

* [publ-hol.R](publ-hol.R) Creates indicators for public and school holidays
* [zh_indicators.R](zh_indicators.R) Create policy indicators
* [cont-mat.R](cont-mat.R) Creates the time-varying contact matrices
* [temperature.R](temperature.R) Gets information needed for the temperature covariate
* [data-zurich.R](data-zurich.R) Population data and case counts

**NB the case data (the surveillance time series used) is confidential and so
_data-zurich.R_ should provide an error stating the data is not found. For this
reason, hhh4 model objects are also not saved as they contain stsObj and also
no prediction objects as they contain the realisations matrix**

## Analysis

The following scripts are used for analysis

* [analysis.R](analysis.R) Modelling and examining the two scenarios (this script calls [step1-fit-models.R](step1-fit-models.R))
* [sensitivity-lag.R](sensitivity-lag.R) Using alternative lag distributions

## Makefile

It should also be possible to execute the provided makefile to run the analysis

## Subfolders

Additionally, the following folders are included in this repository

* [data](data) Contains input data as well as .rda objects created by us
* [tables](tables) Contains tables in .tex format
* [figures](figures) Contains figures in .pdf format

The code was run in the environment described in [sessioninfo.txt](sessioninfo.txt)
